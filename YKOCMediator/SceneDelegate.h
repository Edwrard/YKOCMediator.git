//
//  SceneDelegate.h
//  YKOCMediator
//
//  Created by edward on 2021/5/24.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

