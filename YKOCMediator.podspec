
Pod::Spec.new do |spec|
  spec.name         = "YKOCMediator"
  spec.version      = "1.0.0"
  spec.summary      = "A short description of YKOCMediator."
  spec.description      = <<-DESC
                       组件主控
                       DESC
                       
  spec.homepage     = "https://gitee.com/Edwrard/YKOCMediator"
  
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  
  spec.author       = { "edward" => "534272374@qq.com" }
  spec.source       = { :git => "https://gitee.com/Edwrard/YKOCMediator.git",:tag => spec.version.to_s }
  spec.ios.deployment_target = "10.0"
  spec.framework  = "Foundation"
  
  spec.dependency "YKOCExection/Core"
  spec.source_files = 'YKOCMediator/Classes/*.{h,m}'
  spec.resources = 'YKOCMediator/Resources/**/*'
  
  
end
