//
//  YKNoActionResponseViewController.m
//  YK_BaseMediator
//
//  Created by edward on 2021/4/14.
//

#import "YKNoActionResponseViewController.h"


@interface YKNoActionResponseViewController ()
///
@property (nonatomic, strong, readwrite) UIImageView *noDataImageView;
///
@property (nonatomic, strong, readwrite) UILabel *noDataLabel;
///
@property (nonatomic, strong, readwrite) UIButton *goBackButton;
@end

@implementation YKNoActionResponseViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.modalPresentationStyle = UIModalPresentationPageSheet;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self bindData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    UIEdgeInsets safeEdge = UIEdgeInsetsZero;
    if (@available(iOS 11.0, *)) {
        safeEdge = self.view.safeAreaInsets;
    }
    self.noDataImageView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - safeEdge.left - safeEdge.right - 200)/2, ([UIScreen mainScreen].bounds.size.height - safeEdge.top - safeEdge.bottom - 200)/2 - 100, 200, 200);
    [self.noDataLabel sizeToFit];
    self.noDataLabel.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - safeEdge.left - safeEdge.right - (self.noDataLabel.bounds.size.width))/2, CGRectGetMaxY(self.noDataImageView.frame) + 20, self.noDataLabel.bounds.size.width, self.noDataLabel.bounds.size.height);
    [self.goBackButton sizeToFit];
    self.goBackButton.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - safeEdge.left - safeEdge.right - (self.goBackButton.bounds.size.width))/2, CGRectGetMaxY(self.noDataLabel.frame) + 20, self.goBackButton.bounds.size.width, self.goBackButton.bounds.size.height);
    
    
    
}

- (void)setupUI
{
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self.view addSubview:self.noDataImageView];
    [self.view addSubview:self.noDataLabel];
    [self.view addSubview:self.goBackButton];
    
}

- (void)back:(id)sender {
    if (self.navigationController) {
        if (self.navigationController.presentingViewController &&
            self.navigationController.viewControllers.count == 1) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)bindData
{
    
}

//MARK: get/set

- (UIImageView *)noDataImageView
{
    if (!_noDataImageView) {
        _noDataImageView = [[UIImageView alloc] init];
        _noDataImageView.image = [self ykocmediator_imageNamed:@"ic_networkUnavailable"];
    }
    return _noDataImageView;
}

- (UILabel *)noDataLabel
{
    if (!_noDataLabel) {
        _noDataLabel = [[UILabel alloc] init];
        _noDataLabel.text = @"功能正在开发中\r\n敬请期待";
        _noDataLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        _noDataLabel.numberOfLines = 0;
        _noDataLabel.textAlignment = NSTextAlignmentCenter;
        [_noDataLabel sizeToFit];
    }
    return _noDataLabel;
}

- (UIButton *)goBackButton
{
    if (!_goBackButton) {
        _goBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_goBackButton setTitle:@"点击返回" forState:UIControlStateNormal];
        [_goBackButton setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
        _goBackButton.titleLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
        [_goBackButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        [_goBackButton sizeToFit];
    }
    return _goBackButton;
}

- (nullable UIImage *)ykocmediator_imageNamed:(NSString *)name {
    
    if(name && ![name isEqualToString:@""]){
        
        NSString *ClassName = @"YKMediator";
        NSString *ResourceName = @"YKOCMediator";
        
        NSBundle *bundle = [NSBundle bundleForClass:NSClassFromString(ClassName)];
        NSURL *url = [bundle URLForResource:ResourceName withExtension:@"bundle"];
        
        if (url) {
            NSBundle *imageBundle = [NSBundle bundleWithURL:url];
            
            NSString *imageName = nil;
            CGFloat scale = [UIScreen mainScreen].scale;
            if (ABS(scale-3) <= 0.001){
                imageName = [NSString stringWithFormat:@"%@@3x",name];
            }else if(ABS(scale-2) <= 0.001){
                imageName = [NSString stringWithFormat:@"%@@2x",name];
            }else{
                imageName = name;
            }
            UIImage *image = [UIImage imageWithContentsOfFile:[imageBundle pathForResource:imageName ofType:@"png"]];
            if (!image) {
                image = [UIImage imageWithContentsOfFile:[imageBundle pathForResource:name ofType:@"png"]];
                if (!image) {
                    image = [UIImage imageNamed:name];
                    if (!image) {
                        NSBundle *bundle = [NSBundle bundleForClass:NSClassFromString(ClassName)];
                        image = [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
                    }
                }
            }
            
            return image;
        }else {
            NSBundle *bundle = [NSBundle bundleForClass:NSClassFromString(ClassName)];
            return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
        }
    }
    
    return nil;
    
}

@end
