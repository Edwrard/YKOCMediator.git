//
//  YKNoActionResponseViewController.h
//  YK_BaseMediator
//
//  Created by edward on 2021/4/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKNoActionResponseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
