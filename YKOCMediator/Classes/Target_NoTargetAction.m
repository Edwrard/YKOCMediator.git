//
//  Target_NoTargetAction.m
//  LMCommonModule
//
//  Created by Arclin on 2019/7/23.
//

#import "Target_NoTargetAction.h"
#import "YKNoActionResponseViewController.h"
//#import <LMAppSetting.h>

@implementation Target_NoTargetAction

- (void)Action_response:(NSDictionary *)params {
//    if([LMAppSetting sharedInstance].currentEnvironment == LMAppTargetClip) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"kShowDownloadAlert" object:nil];
//        return nil;
//    }
    NSDictionary *originParams = params[@"originParams"];
    NSString *targetString = params[@"targetString"];
    NSString *selectorString = params[@"selectorString"];
    NSLog(@"%@ %@ %@",originParams,targetString,selectorString);
    UIViewController *vc = [[YKNoActionResponseViewController alloc] init];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    return;
}

@end
