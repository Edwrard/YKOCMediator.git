//
//  YKMediator.h
//  YKMediator
//
//  Created by casa on 16/3/13.
//  Copyright © 2016年 casa. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YKMediator : NSObject

+ (instancetype _Nonnull)sharedInstance;


// 本地组件调用入口
// exp:  target/action
- (id _Nullable)openUrl:(NSString * _Nonnull)urlStr params:(NSDictionary *_Nullable)params shouldCacheTarget:(BOOL)shouldCacheTarget;

// 远程App调用入口
- (id _Nullable)performActionWithUrl:(NSURL * _Nullable)url completion:(void(^_Nullable)(NSDictionary * _Nullable info))completion;


- (void)releaseCachedTargetWithFullTargetName:(NSString * _Nullable)fullTargetName;

@end
  
// 简化调用单例的函数
YKMediator* _Nonnull CT(void);
