# YKOCMediator

[![Platform](https://img.shields.io/badge/platform-iOS-red.svg)](https://developer.apple.com/iphone/index.action) [![Language](http://img.shields.io/badge/language-OC-yellow.svg?style=flat )](https://en.wikipedia.org/wiki/Objective-C) [![License](https://img.shields.io/badge/license-MIT-blue.svg)](http://mit-license.org) [![CocoaPods Compatible](https://img.shields.io/badge/cocoapod-last-green.svg)](https://gitee.com/Edwrard/YKOCMediator)

README.md

## How To Get Started

- add source at the top of the 'Podfile'
```
source 'https://gitee.com/Edwrard/YKSpec.git'  
source 'https://github.com/CocoaPods/Specs.git'
```
- delete the row of 'use_frameworks!'

## Communication

- If you **need help**, use [Stack Overflow](https://stackoverflow.com/). 
- If you'd like to **ask a general question**, use [Stack Overflow](https://stackoverflow.com/).
- If you **found a bug**, _and can provide steps to reliably reproduce it_, open an issue.
- If you **have a feature request**, open an issue.
- If you **want to contribute**, submit a pull request.

## Installation
YKOCMediator supports multiple methods for installing the library in a project.

## Installation with CocoaPods

To integrate YKOCMediator into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'YKOCMediator'
```


